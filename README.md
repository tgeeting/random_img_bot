# **Random Image Generator** #
## Author: Travis Geeting ##
Using 2 lists of subjects, this program performs a google image search using a small given dictionary
to find 2 images, a subject and a canvas, and combines them to create a
squared aspect ratio collage image which is then sent to a recipient 
through email.
### Environment Variables: ###
* SENDER: a gmail account name from which the collage will be sent
* RECIPIENT: an email account that will receive the collage
* PASSWORD: the password for the gmail account used for SENDER
### Run: ###
```
#!python

python randImgApp.py
```
###View:###
The results of this project can be viewed at [imagesearchcurator.tumblr.com](imagesearchcurator.tumblr.com)