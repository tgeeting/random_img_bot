from PIL import Image
import imgDictionary
import imgEmail
import urllib
import urllib2
import simplejson
import cStringIO
from random import randint, choice, shuffle
import time


class randImgApp:
    '''
        initialize the 3 images used
    '''
    def __init__(self):
        self.collage = Image.new('RGBA',(10,10),255)  #the final collage image
        self.subject = Image.new('RGBA',(10,10),255)  #the subject image placed in the foreground
        self.canvas = Image.new('RGBA',(10,10),255)   #the wallpaper image placed in the background
        self.sub_term = '' #term used to search for subject image
        self.can_term = '' #term used to search for canvas image
        self.sub_dict = imgDictionary.subjectDictionary() #dictionary used to supply subject search terms
        self.can_dict = imgDictionary.canvasDictionary() #dictionary used to supply canvas search terms
        self.tags = '' #tags used when posting the image
        
    '''
        retrieves image file from a google image search using a term string with no spaces
    '''
    def imgSearch(self, term):
        fetcher = urllib2.build_opener()
        searchTerm = term
        startIndex = randint(0,60)
        searchUrl = "http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + searchTerm + "&start=" + str(startIndex)
        f = fetcher.open(searchUrl)
        deserialized_output = simplejson.load(f)

        imageUrl = deserialized_output['responseData']['results'][randint(0,3)]['unescapedUrl']
        file = cStringIO.StringIO(urllib.urlopen(imageUrl).read())
        return file

    '''
        attempts to retrieve an image that Pillow is able to open
            without error and returns the first one it finds
    '''
    def validSearch(self, term):
        while True:
            try:
                img = Image.open(self.imgSearch(term))
            except IOError:
                pass
            else:
                return img
    '''
        returns the relation of width to height for use in
            cropping and resizing
    '''
    def determineFrame(self, size):
        frame = size
        if(frame[0] <= frame[1]): #width < height
            return (0,1)
        else: #height < width
            return (1,0)

    '''
        crops the canvas into a square
    '''
    def cropCanvas(self):
        canSize = self.canvas.size
        index = self.determineFrame(canSize)
        
        diff = int((canSize[index[1]]-canSize[index[0]])/2)
        left = int(index[0]*diff)
        upper = int(index[1]*diff)
        right = int(canSize[0]-(index[0]*diff))
        lower = int(canSize[1]-(index[1]*diff))
        box = (left, upper, right, lower)
        self.canvas = self.canvas.crop(box)

    '''
        trims the canvas to make sure its in a square
    '''
    def trimCanvas(self):
        width, height = self.canvas.size[0], self.canvas.size[1]
        if width != height:
            if width > height:
                self.canvas = self.canvas.crop((0,0,width-(width-height),height))
            else:
                self.canvas = self.canvas.crop((0,0,width,height-(height-width)))

    '''
        cleans the whites of the subject image to make them
            transparent
    '''
    def cleanSubject(self):
        if self.subject.mode != 'RGBA':
            self.subject = self.subject.convert('RGBA')
            datas = self.subject.getdata()
            newData = []
            for item in datas:
                if item[0] > 253 and item[1] > 253 and item[2] > 253:
                    newData.append((255, 255, 255, 0))
                else:
                    newData.append(item)
            self.subject.putdata(newData)

    '''
        returns copy of subject image resized fit in relation to
            the canvas image
    '''
    def resizeSubject(self):
        subSize = self.subject.size
        canSize = self.canvas.size
        index = self.determineFrame(subSize)
        newSize = [0, 0]
        
        dimension = int(.63*subSize[index[1]]*canSize[index[0]]/subSize[index[0]])
        newSize[index[0]] = int(.63*canSize[index[0]])
        newSize[index[1]] = dimension
        
        self.subject = self.subject.resize(tuple(newSize),Image.ANTIALIAS)
        self.subject.thumbnail(canSize, Image.ANTIALIAS)
    
    '''
        prepares canvas for assembling collage
    '''
    def prepareCanvas(self):
        self.can_term = choice(self.can_dict)
        shuffle(self.can_dict)
        self.canvas = self.validSearch(self.can_term).convert('RGBA')
        self.cropCanvas()
        self.trimCanvas()
        
    '''
        prepares subject for assembling collage
    '''
    def prepareSubject(self):
        self.sub_term = choice(self.sub_dict)
        shuffle(self.sub_dict)
        self.subject = self.validSearch('%27'+self.sub_term+'%20png')
        self.cleanSubject()
        self.resizeSubject()

    '''
        assembles the collage
    '''
    def assembleCollage(self):
        subSize = self.subject.size
        canSize = self.canvas.size
        box = (int((canSize[0]-subSize[0])/2), int((canSize[1]-subSize[1])/2))
        self.collage = self.canvas.copy()
        self.collage.paste(self.subject,box,self.subject)
        self.collage = self.collage.convert('RGBA')
        self.collage = self.collage.resize((540,540),Image.ANTIALIAS)
    
    '''
        creates list of tags to be sent with image
    '''
    def createTags(self):
        self.tags = '#imagesearchcurator #images #photos'+' #'+self.sub_term+' #'+self.can_term
    
    '''
        starts the app
    '''
    def start(self):
        while True:
            self.prepareCanvas()
            self.prepareSubject()
            self.assembleCollage()
            self.createTags()
            self.collage.save('images/collage.png','PNG')
            imgEmail.sendMail('images/collage.png', self.tags)
            time.sleep(1200)
        
if __name__ == "__main__":
    app = randImgApp()
    app.start()
    
