from random import choice, randint

def subjectDictionary():
    subjects = ['vegetable','human','fruit','dog','cat','fish','fence',
    'snake','weapon','sword','bread','celebrity','mannequin','water','monster','dew',
    'sand','hair','dust','coupon','bottle','sneaker','fan','pastry','pills','drops',
    'foot','nail','internet','smoke','grass','cube','post','image','plant','insect',
    'hat','spider','tv','photo','technology','cup','kimkardashian','jimmyfallon','bobbypin','safetypin',
    'key','marina','knife','sandwich','chanel','saintlaurent','michaelkors','louisvuitton','louisck','mouse',
    'shialabeouf','weed','nike','adidas','supreme','gum','light','dark','wind','manga','paper','evil',
    'alphabet','typeface','calligraphy','athlete','seinfeld','guyfieri','hotdog','stockphoto','alexanderwang',
    'olympics','larryking','billnye','drake','candle','tablet','trash','pile','ring','polo','ralphlauren',
    'pepsi','coke','chips','wheel','casino','broom','airplane','nickiminaj','beyonce','rihanna',
    'dennys','mcdonalds','burgerking','starbucks','money','stevebuscemi','dunk','shot','towels']
    return subjects
    
def canvasDictionary():
    canvases = ['background','grass','desktop','rug','carpet','screensaver','farm',
    'poster','portrait','landscape','ocean','mountain','store','mural','shelves',
    'restaurant','city','room','field','dungeon','jail','color','photo','wallpaper',
    'wall','bricks','air','sky','produce','internet','website','frame','technology',
    'concrete','macadam','stock','stockphoto','zoo','park','mall','dennys','mcdonalds','burgerking',
    'starbucks','paris','newyork','losangeles','hell','heaven','purgatory','renaissance','construction',
    'skateboarding','money','trash','garbage','laundry','beach','store']
    return canvases